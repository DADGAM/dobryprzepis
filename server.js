const express = require('express'),
  mongoose = require('mongoose'),
  bodyParser = require('body-parser'),
  path = require('path');

let db = mongoose.connect('mongodb://test:test@ds135196.mlab.com:35196/dobry');

let Recipe = require('./server/models/recipeModel'),
  User = require('./server/models/userModel');

let app = express(),
  port = process.env.PORT || 8000;
app.use(express.static(path.join(__dirname, 'dist')));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

let recipeRouter = require('./server/routes/recipeRouter')(Recipe);
userRouter = require('./server/routes/userRouter')(User);

app.use('/api/recipe', recipeRouter);
app.use('/api/user', userRouter);
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

app.listen(port, function () {
  console.log('gulp is running my app on' + port + 'port');
});
