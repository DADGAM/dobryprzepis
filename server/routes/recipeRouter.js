const express = require('express'),
  recipeRouter = express.Router(),
  jwt = require('jsonwebtoken');

const routes = (Recipe) => {
  const recipeController = require('../controllers/recipeController')(Recipe);

  recipeRouter.use('/:id', (req, res, next) => recipeController.getRecipe(req, res, next));

  recipeRouter.route('/')
    .get((req, res) => recipeController.showAll(req, res));
  recipeRouter.route('/:id')
    .get((req, res) => recipeController.showOne(req, res));

  //dla zalogowanych 
  recipeRouter.use('/', (req, res, next) => recipeController.isAuthorized(req, res, next));

  recipeRouter.route('/')
    .post((req, res) => recipeController.add(req, res));
  recipeRouter.route('/:id')
    .patch((req, res) => recipeController.update(req, res))
    .delete((req, res) => recipeController.remove(req, res));

  recipeRouter.route('/fav/:id')
    .patch((req, res) => recipeController.addToFavorite(req, res));


  return recipeRouter;
};
module.exports = routes;
