const express = require('express'),
  userRouter = express.Router();

let routes = (User) => {
  let userController = require('../controllers/userController')(User);

  userRouter.route('/')
    .get(userController.showAll)
    .post(userController.add);

  userRouter.use('/:id', userController.getUser);
  userRouter.route('/:id')
    .get(userController.showOne)

  userRouter.route('/signin')
    .post((req, res) => userController.signIn(req, res));


  userRouter.use('/', (req, res, next) => userController.isAuthorized(req, res, next));
  userRouter.route('/:id')
    .patch(userController.update)
    .delete(userController.remove);

  return userRouter;
};
module.exports = routes;
