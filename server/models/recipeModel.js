let mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = mongoose.Schema.Types.ObjectId,
  User = require('./userModel');

let recipeModel = new Schema({
  title: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  author: ObjectId,
  fav: [ObjectId],
});

recipeModel.post('remove', (recipe) => {
  User.findById(recipe.author, (error, user) => {
    user.ownArticles.pull(recipe);
    user.save();
  })
})
module.exports = mongoose.model('Recipe', recipeModel);
