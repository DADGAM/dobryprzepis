const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = mongoose.Schema.Types.ObjectId;

const userModel = new Schema({
  login: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  lastname: {
    type: String,
    required: false,
  },
  mail: {
    type: String,
    required: true,
  },
  article: [ObjectId],
  ownArticles: [ObjectId],
  fav: [ObjectId],
});
module.exports = mongoose.model('User', userModel);
