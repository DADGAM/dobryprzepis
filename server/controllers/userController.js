const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Recipe = require('../models/recipeModel');

const userController = (User) => {
  const add = (req, res) => {
    const newUser = new User({
      login: req.body.login,
      password: bcrypt.hashSync(req.body.password, 10),
      name: req.body.name,
      lastname: req.body.lastname,
      mail: req.body.mail,
    });

    newUser.save((error, user) => {
      if (error) {
        console.log(error);
      } else {
        res.status(201);
        res.send(user);
      }
    });
  };
  const showAll = (req, res) => {
    User.find({}, (error, users) => {
      if (error) {
        res.status(500).send(error);
      } else {
        console.log(users);
        res.status(200).json(users);
      }
    });
  };
  const getUser = (req, res, next) => {
    User.findById(req.params.id, (error, user) => {
      if (error) {
        res.status(500).send(error);
      } else if (user) {
        req.user = user;
        next();
      } else {
        res.status(404).send('No user');
      }
    });
  };

  const showOne = (req, res) => {
    res.status(200).json(req.user);
  };

  const update = (req, res) => {
    if (req.body._id) {
      delete req.body._id;
    }
    for (let property in req.body) {
      req.user[property] = req.body[property];
    }
    req.user.save((error, user) => {
      if (error) {
        res.status(500).send(error);
      } else {
        res.json(user)
      }
    });
  };

  const remove = (req, res) => {
    req.user.remove((error, user) => {
      if (error) {
        res.status(500).send(error);
      } else {
        res.status(204).send(user);
      }
    });
  };

  const signIn = (req, res) => {
    User.findOne({
      login: req.body.login
    }, (error, user) => {
      if (error) {
        return res.status(500).json('error');
      }
      if (!user) {
        return res.status(401).json('Invalid login credentials')
      }
      if (!bcrypt.compareSync(req.body.password, user.password)) {
        return res.status(401).json('Invalid login credentials')
      }

      const token = jwt.sign({
        user: user
      }, 'sialalalala', {
        expiresIn: 7200,
      });
      res.status(200).json({
        token: token,
        userId: user._id,
      });

    })
  }

  const isAuthorized = (req, res, next) => {
    jwt.verify(req.query.token, 'sialalalala', (error, decoded) => {
      if (error) {
        return res.status(401).json({
          title: 'No Athentication',
          error: error,
        });
      }
      next();
    });
  };


  return {
    signIn: signIn,
    add: add,
    update: update,
    remove: remove,
    showAll: showAll,
    showOne: showOne,
    getUser: getUser,
    isAuthorized: isAuthorized,
  };
};
module.exports = userController;
