const jwt = require('jsonwebtoken'),
  User = require('../models/userModel');

const recipeController = (Recipe) => {
  const getRecipe = (req, res, next) => {
    Recipe.findById(req.params.id, (error, recipe) => {
      if (error) {
        return res.status(500).send(error);
      } else if (recipe) {
        req.recipe = recipe;
        next();
      } else {
        return res.status(404).send('No recipe!');
      }
    });
  };

  const add = (req, res) => {
    console.log(req);
    const decoded = jwt.decode(req.query.token);
    User.findById(decoded.user._id, (error, user) => {
      if (error) {
        return res.status(500).json({
          title: 'Error occured',
          error: error,
        });
      }

      const newRecipe = new Recipe({
        title: req.body.title,
        category: req.body.category,
        content: req.body.content,
        author: user,
      });

      newRecipe.save((error, recipe) => {
        if (error) {
          return res.status(201).json({
            title: 'Unexpected Error',
            error: error,
          });
        }
        user.ownArticles.push(recipe);
        user.save();
        res.status(201).send({
          title: 'Recipe saved!',
          recipe: recipe,
        });
      });
    });
  };

  const showAll = (req, res) => {
    Recipe.find({}, (error, recipes) => {
      if (error) {
        res.status(500).send(err);
      } else {
        console.log(recipes);
        res.status(200).json(recipes);
      }
    });
  };

  const showOne = (req, res) => {
    console.log(req.recipe);
    res.status(200).json(req.recipe);
  };

  const update = (req, res) => {
    const decoded = jwt.decode(req.query.token);
    if (req.body._id) {
      delete req.body._id;
    }

    for (let property in req.body) {
      req.recipe[property] = req.body[property];
    }

    if (req.recipe.author != decoded.user._id) {
      return res.status(401).json({
        title: 'No authenticated',
        error: {
          message: 'User do not match'
        }
      });
    }

    req.recipe.save((error, recipe) => {
      if (error) {
        return res.status(500).json(error);
      }
      return res.json(recipe);
    });
  };

  const remove = (req, res) => {
    const decoded = jwt.decode(req.query.token);
    if (req.recipe.author != decoded.user._id) {
      return res.status(401).json({
        title: 'No authenticated',
        error: {
          message: 'User do not match'
        }
      });
    }
    req.recipe.remove((error, result) => {
      if (error) {
        return res.status(500).json({
          title: 'An error occurred',
          error: error,
        });
      }
      res.status(200).json({
        message: 'Delete message',
        obj: result,
      });
    });
  };

  const addToFavorite = (req, res) => {
    const decoded = jwt.decode(req.query.token);
    req.recipe.fav.push(decoded.user._id);
    req.recipe.save((error, recipe) => {
      if (error) {
        return res.status(500).json({
          title: 'An error occurred',
          error: error,
        });
      }
      User.findByIdAndUpdate(
        decoded.user._id, {
          $set: {
            fav: req.recipe._id
          }
        },
        (error, user) => {
          if (error) {
            return res.status(500).json({
              title: 'An error occurred',
              error: error,
            });
          }
        })
    });
  }

  const isAuthorized = (req, res, next) => {
    jwt.verify(req.query.token, 'sialalalala', (error, decoded) => {
      if (error) {
        return res.status(401).json({
          title: 'No Athentication',
          error: error,
        });
      }
      next();
    });
  };

  return {
    isAuthorized: isAuthorized,
    add: add,
    update: update,
    remove: remove,
    showAll: showAll,
    showOne: showOne,
    getRecipe: getRecipe,
    addToFavorite: addToFavorite,
  }
};

module.exports = recipeController;
