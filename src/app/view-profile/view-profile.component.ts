import { Users } from './../users-list/users';
import { UsersService } from './../users.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit {

  user: Users;

  constructor(
    private route: ActivatedRoute,
    private service: UsersService,
  ) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        if (params['id']) {
          this.service.getOne(params['id'])
            .subscribe(user => this.user = user);
        }
        if (!params['id'] && localStorage.getItem('userId')) {
          this.service.getOne(localStorage.getItem('userId'))
            .subscribe(user => this.user = user);
        }
      });
  }

}
