import { Recipe } from './../recipe-list/recipe';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-user-recipe-details',
  templateUrl: './user-recipe-details.component.html',
  styleUrls: ['./user-recipe-details.component.css']
})
export class UserRecipeDetailsComponent implements OnInit {

  recipe: Recipe;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: RecipeService,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params['id']) {
        this.service.getOne(params['id'])
          .subscribe(recipe => this.recipe = recipe);
      }
    });
  }

  addToFavorite() {
    return this.service.addToFavorite(this.recipe._id);
  }
}