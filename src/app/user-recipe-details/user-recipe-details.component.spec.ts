import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRecipeDetailsComponent } from './user-recipe-details.component';

describe('UserRecipeDetailsComponent', () => {
  let component: UserRecipeDetailsComponent;
  let fixture: ComponentFixture<UserRecipeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRecipeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRecipeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
