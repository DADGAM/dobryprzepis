import { Recipe } from './../recipe-list/recipe';
import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../recipe.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-user-recipe-list',
  templateUrl: './user-recipe-list.component.html',
  styleUrls: ['./user-recipe-list.component.css']
})
export class UserRecipeListComponent implements OnInit {

  recipes: Recipe[];
  fav: boolean = false;
  own: boolean = false;

  constructor(
    private service: RecipeService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.fav = params['fav'] || false;
      this.own = params['own'] || false;
    });

    this.service.getAll()
      .subscribe(recipes => this.recipes = recipes);
  }

  show(recipe: Recipe) {
    const userId = localStorage.getItem('userId');
    if (this.fav) {
      if (recipe.favorite.indexOf(userId) != -1)
        return true;
      return false;
    }
    if (this.own) {
      if (recipe.author == userId)
        return true;
      return false;
    }
    return true;
  }

  showRecipe(recipe: Recipe) {
    this.router.navigateByUrl('/recipe', {
      queryParams: {
        id: recipe.id,
      }
    });
  }
}
