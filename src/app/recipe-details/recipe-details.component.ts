import { RecipeService } from './../recipe.service';
import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../recipe-list/recipe';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {

  @Input('Recipe') Recipe: Recipe;
  @Input('deleteHandler') deleteHandler: Function;
  @Input('addHandler') addHandler: Function;
  @Input('updateHandler') updateHandler: Function;

  constructor(private service: RecipeService) { }

  ngOnInit() {
    if (!this.Recipe) {
      this.Recipe = new Recipe('', '', '');
    }
  }
  deleteRecipe(Recipe: Recipe) {
    const index = this.deleteHandler(Recipe._id);
    this.service.delete(Recipe._id)
      .subscribe(null, (error) => {
        this.addHandler(Recipe, index);
      })
  }

  updateRecipe(Recipe: Recipe) {
    const oldRecipe = this.updateHandler(Recipe);
    this.service.update(Recipe)
      .subscribe(null, (error) => {
        this.updateHandler(oldRecipe);
      })
  }
  addRecipe(Recipe: Recipe) {
    this.service.create(Recipe)
      .subscribe(null, error => console.log(error));
  }
}
