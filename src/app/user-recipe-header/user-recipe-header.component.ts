import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-recipe-header',
  templateUrl: './user-recipe-header.component.html',
  styleUrls: ['./user-recipe-header.component.css']
})
export class UserRecipeHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
