import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRecipeHeaderComponent } from './user-recipe-header.component';

describe('UserRecipeHeaderComponent', () => {
  let component: UserRecipeHeaderComponent;
  let fixture: ComponentFixture<UserRecipeHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRecipeHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRecipeHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
