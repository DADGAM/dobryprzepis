import { Component, OnInit, Input } from '@angular/core';
import { Users } from '../users-list/users';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {

  @Input('User') User: Users;

  constructor(private service: UsersService) { }

  ngOnInit() {
    this.User = new Users('', '', '', '', '');
  }
  addUser(User: Users) {
    this.service.create(User)
      .subscribe(null, error => console.log(error));
  }

}
