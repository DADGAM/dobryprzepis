import { RouterModule } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { RecipeService } from './recipe.service';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersService } from './users.service';
import { HeaderComponent } from './header/header.component';
import { UserHeaderComponent } from './user-header/user-header.component';
import { RecipeHeaderComponent } from './recipe-header/recipe-header.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { SigninFormComponent } from './signin-form/signin-form.component';
import { LogoutComponent } from './logout/logout.component';
import { ViewRecipeComponent } from './view-recipe/view-recipe.component';
import { ManageRecipeComponent } from './manage-recipe/manage-recipe.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { UserRecipeListComponent } from './user-recipe-list/user-recipe-list.component';
import { UserRecipeDetailsComponent } from './user-recipe-details/user-recipe-details.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { UserRecipeHeaderComponent } from './user-recipe-header/user-recipe-header.component';


@NgModule({
  declarations: [
    AppComponent,
    RecipeListComponent,
    UsersListComponent,
    HeaderComponent,
    UserHeaderComponent,
    RecipeHeaderComponent,
    UserDetailsComponent,
    RecipeDetailsComponent,
    SignupFormComponent,
    SigninFormComponent,
    LogoutComponent,
    ViewRecipeComponent,
    ManageRecipeComponent,
    MainHeaderComponent,
    UserRecipeListComponent,
    UserRecipeDetailsComponent,
    ViewProfileComponent,
    UserRecipeHeaderComponent,
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '', component: HeaderComponent, children: [
          {
            path: 'recipes', component: UserRecipeHeaderComponent, children: [
              { path: 'list', component: UserRecipeListComponent },
              { path: 'add', component: UserRecipeDetailsComponent },
            ]
          }]
      },
      {
        path: 'users', component: UserHeaderComponent, children: [
          { path: 'list', component: UsersListComponent },
          { path: 'add', component: UserDetailsComponent }
        ]
      },
      {
        path: 'signup', component: SignupFormComponent,
      },
      {
        path: 'signin', component: SigninFormComponent,
      },
      {
        path: 'logout', component: LogoutComponent,
      },
    ]),
  ],
  providers: [
    UsersService,
    RecipeService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
