import { UsersService } from './../users.service';
import { Component, OnInit } from '@angular/core';
import { Users } from './users';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users: Users[];
  selectedUser: Users;


  constructor(private service: UsersService) { }

  ngOnInit() {
    this.service.getAll()
      .subscribe(users => this.users = users);
  }

  remove(user) {
    let index = this.users.indexOf(user);
    this.users.splice(index, 1);

    this.service.delete(user._id)
      .subscribe(null, (error) => {
        this.users.splice(index, 0, user);
      })
  }
  selectUser(user: Users) {
    this.selectedUser = user;
  }
  getIndexOfUser(userId) {
    return this.users.findIndex((user) => {
      return user._id === userId;
    })
  }

  deleteUser(userId) {
    const index = this.getIndexOfUser(userId);
    if (index !== -1) {
      this.users.splice(index, 1);
      this.selectedUser = null;
    }
    return index;
  }
  addUser(user: Users, index?: number) {
    if (index) {
      this.users.splice(index, 0, user);
    }
  }
  updateUser(user: Users) {
    const index = this.getIndexOfUser(user._id);
    if (index !== -1) {
      this.users[index] = user;
      this.selectedUser = user;
    }
    return this.users;
  }

}
