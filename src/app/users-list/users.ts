export class Users {
    constructor(
        public login: string,
        public password: string,
        public name: string,
        public lastname: string,
        public mail: string,
        public favorite?: string,
        public article?: string,
        public id?: string,
    ) { }

    get _id() {
        return this.id;
    }
}