import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import { Users } from './users-list/users';

@Injectable()
export class UsersService {

  url = 'http://localhost:8000/api/user';

  constructor(private http: Http) { }

  getAll() {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.get(this.url + token)
      .map(response => response.json())
      .catch(this.handleError);
  }

  getOne(id: string) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.get(this.url + '/' + id + token, options)
      .map(response => response.json())
      .catch(this.handleError);
  }
  create(user) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.post(this.url + token, JSON.stringify(user), options)
      .catch(this.handleError);
  }
  update(user) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const body = JSON.stringify(user);
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.patch(this.url + '/' + user._id + token,
      body, options)
      .catch(this.handleError);
  }
  delete(id) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.delete(this.url + '/' + id + token, options)
      .catch(this.handleError);
  }
  signin(user: Users) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(this.url + '/signin', JSON.stringify(user), options)
      .map(response => response.json() as String)
      .catch(this.handleError);
  }
  isLoggedIn() {
    return localStorage.getItem('token') != null;
  }


  private handleError(error: Response) {
    if (error.status === 404) {
      return Observable.throw('Not found');
    }
    if (error.status === 400) {
      return Observable.throw('Bad request');
    }
    return Observable.throw('Błąd aplikacji');
  }

}
