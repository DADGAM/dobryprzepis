import { Recipe } from './../recipe-list/recipe';
import { UsersService } from './../users.service';
import { RecipeService } from './../recipe.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Users } from '../users-list/users';

@Component({
  selector: 'app-manage-recipe',
  templateUrl: './manage-recipe.component.html',
  styleUrls: ['./manage-recipe.component.css']
})
export class ManageRecipeComponent implements OnInit {
  recipe: Recipe;
  users: Users;

  constructor(
    private recipeService: RecipeService,
    private usersService: UsersService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.recipeService.getOne(params.id)
          .subscribe(recipe => this.recipe = recipe);
      });
    this.usersService.getAll()
      .subscribe(users => this.users = users);
  }
}
