import { UsersService } from './../users.service';
import { Users } from './../users-list/users';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signin-form',
  templateUrl: './signin-form.component.html',
  styleUrls: ['./signin-form.component.css']
})
export class SigninFormComponent implements OnInit {

  form = new FormGroup({
    login: new FormControl('',
      [
        Validators.required,
      ]),
    password: new FormControl('',
      [
        Validators.required,
      ])
  });

  constructor(private service: UsersService, private router: Router) { }

  ngOnInit() {

  }
  signin() {
    const user = new Users(
      this.login.value,
      this.password.value,
      '',
      '',
      '',
    );

    this.service.signin(user)
      .subscribe(
      data => {
        localStorage.setItem('token', data.token);
        localStorage.setItem('userId', data.userId);
        this.router.navigateByUrl('/');

      },
      error => console.log(error)
      );
  }

  get login() {
    return this.form.get('login');
  }

  get password() {
    return this.form.get('password');
  }
}
