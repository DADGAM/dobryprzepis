import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';

@Injectable()
export class RecipeService {

  url = 'http://localhost:8000/api/recipe';

  constructor(private http: Http) { }

  getAll() {
    return this.http.get(this.url)
      .map(response => response.json())
      .catch(this.handleError);
  }
  getOne(recipeId: string) {
    return this.http.get(this.url + '/' + recipeId)
      .map(response => response.json())
      .catch(this.handleError);
  }
  create(recipe) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.post(this.url + token, JSON.stringify(recipe), options)
      .catch(this.handleError);
  }

  update(recipe) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const body = JSON.stringify(recipe);
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.patch(this.url + '/' + recipe._id + token, body, options)
      .catch(this.handleError);
  }

  delete(id) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.delete(this.url + '/' + id + token, options)
      .catch(this.handleError);
  }

  addToFavorite(recipeId: String) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const token = localStorage.getItem('token')
      ? '?token=' + localStorage.getItem('token')
      : '';
    return this.http.patch(this.url + '/fav/' + recipeId + token, options)
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    if (error.status === 404) {
      return Observable.throw('Not found');
    }
    if (error.status === 400) {
      return Observable.throw('Bad request');
    }
    return Observable.throw('Błąd aplikacji');
  }

}
