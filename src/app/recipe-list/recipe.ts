export class Recipe {
    constructor(
        public title: string,
        public category: string,
        public content: string,
        public author?: string,
        public id?: string,
        public favorite?: string[],
    ) { }

    get _id() {
        return this.id;
    }
}