import { RecipeService } from '../recipe.service';
import { Component, OnInit } from '@angular/core';
import { Recipe } from './recipe';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[];
  selectedRecipe: Recipe;

  constructor(private service: RecipeService) { }

  ngOnInit() {
    this.service.getAll()
      .subscribe(recipes => this.recipes = recipes);
  }
  remove(recipe) {
    let index = this.recipes.indexOf(recipe);
    this.recipes.splice(index, 1);

    this.service.delete(recipe._id)
      .subscribe(null, (error) => {
        this.recipes.splice(index, 0, recipe);
      })
  }
  selectRecipe(recipe: Recipe) {
    this.selectedRecipe = recipe;
  }
  getIndexOfRecipe(recipeId) {
    return this.recipes.findIndex((recipe) => {
      return recipe._id === recipeId;
    })
  }

  deleteRecipe(recipeId) {
    const index = this.getIndexOfRecipe(recipeId);
    if (index !== -1) {
      this.recipes.splice(index, 1);
      this.selectedRecipe = null;
    }
    return index;
  }
  addRecipe(recipe: Recipe, index?: number) {
    if (index) {
      this.recipes.splice(index, 0, recipe);
    }
  }
  updateRecipe(recipe: Recipe) {
    const index = this.getIndexOfRecipe(recipe._id);
    if (index !== -1) {
      this.recipes[index] = recipe;
      this.selectedRecipe = recipe;
    }
    return this.recipes;
  }


}
