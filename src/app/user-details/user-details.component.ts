import { Component, OnInit, Input } from '@angular/core';
import { Users } from '../users-list/users';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  @Input('User') User: Users;
  @Input('deleteHandler') deleteHandler: Function;
  @Input('addHandler') addHandler: Function;
  @Input('updateHandler') updateHandler: Function;

  constructor(private service: UsersService) { }

  ngOnInit() {
    if (!this.User) {
      this.User = new Users('', '', '', '', '');
    }
  }

  deleteUser(User: Users) {
    const index = this.deleteHandler(User._id);
    this.service.delete(User._id)
      .subscribe(null, (error) => {
        this.addHandler(User, index);
      })
  }
  updateUser(User: Users) {
    const oldUser = this.updateHandler(User);
    this.service.update(User)
      .subscribe(null, (error) => {
        this.updateHandler(oldUser);
      })
  }
  addUser(User: Users) {
    this.service.create(User)
      .subscribe(null, error => console.log(error));
  }


}
